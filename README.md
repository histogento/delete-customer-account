Histogento Delete Customer Account Module

Ce module fait partie des modules Histogento développés par Alexis OLIVE.

Installation

Pour installer ce module, vous pouvez utiliser Packagist. Exécutez les commandes suivantes :

```
composer require histogento/delete-customer-account
bin/magento setup:upgrade
bin/magento setup:di:compile
```

Rôle du module

Ce module permet aux clients de supprimer leur compte directement via un lien dans leur espace. Il gère également l'envoi d'un mail aux administrateur du site et au client.

N'hésitez pas à contribuer en signalant les problèmes ou en proposant des améliorations.
